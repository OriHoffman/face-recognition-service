from requests import put, get, post
import random
import json
from pprint import pprint
import concurrent.futures


def gen_vector():
    """
    generates random vector
    :return:
    """
    return [random.random() for _ in range(256)]


def get_closest_vector():
    """
    Sends POST to service
    :return:
    """
    vector = gen_vector()
    res = get('http://localhost:5000/service', json={'vector': vector, 'amount': 3})
    return json.loads(res.text)


def feed_db(amount):
    """
    Feeds db with vectors
    :param amount: amount to feed
    :return:
    """
    input_vectors = [gen_vector() for _ in range(amount)]
    futures = []
    i = 0
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        for vector in input_vectors:
            futures.append(executor.submit(post, url='http://localhost:5000/service',
                                           json={'name': 'george_%s' % i, 'vector': vector}))
        for future in concurrent.futures.as_completed(futures):
            print(future.result())


if __name__ == '__main__':
    feed_db(100)
    docs = json.loads(get_closest_vector()['result'])
    pprint(docs)
