import numpy as np
import logging
import time
from flask_restful import abort
from pymongo import MongoClient
from sorted_collection import SortedCollection
from threading import Lock

MAX_TIME_CACHE = 2
MAX_ITEMS_IN_DB = 10000

cache_to_publish = []


class FaceManager:

    _client = MongoClient('localhost:27017')
    _db = _client.face_recognition
    _lock = Lock()
    _logger = logging.getLogger(__name__)

    @staticmethod
    def check_vector(vector):
        """
        Check vector validity
        :param vector: vector
        :return:
        """
        if len(vector) != 256:
            abort(400, message='vector must be of length 256')

    @staticmethod
    def check_people_count():
        """
        Checks DB has enough space
        :return:
        """
        if FaceManager._db.people.count_documents({}) >= MAX_ITEMS_IN_DB:
            abort(409, message='DB is full')

    @staticmethod
    def get_similar_vectors(vector, amount):
        """
        Returns amount of vector which are most similar to given vector
        :param vector: compare to
        :param amount: how many similar to return
        :return:
        """
        best_matches = SortedCollection(key=lambda x: np.dot(vector, x['vector']))
        cursor = FaceManager._db.people.find({})

        for i in range(amount):  # init best matches with first vectors
            doc = next(cursor, None)
            if doc:
                best_matches.insert(doc)

        thresh_dot_prod = np.dot(best_matches[0]['vector'], vector)  # smallest dotProduct is the threshold

        curr_doc = next(cursor, None)
        while curr_doc:  # iterate over vectors
            curr_dot_prod = np.dot(vector, curr_doc['vector'])

            if curr_dot_prod > thresh_dot_prod:
                best_matches.insert(curr_doc)  # insert new vector
                best_matches.remove(best_matches[0])  # remove smallest dotProd
                thresh_dot_prod = np.dot(best_matches[0]['vector'], vector)  # set new threshold

            curr_doc = next(cursor, None)

        return best_matches

    @staticmethod
    def dump_to_db():
        """
        Thread which dumps new vectors to DB each MAX_TIME_CACHE seconds or when there are enough vectors
        :return:
        """
        global cache_to_publish
        start = time.time()

        while True:
            if len(cache_to_publish) >= MAX_ITEMS_IN_DB / 100 or (time.time() - start > MAX_TIME_CACHE
                                                                  and len(cache_to_publish) > 0):
                try:
                    with FaceManager._lock:
                        FaceManager._db.people.insert_many(cache_to_publish)
                        cache_to_publish = []
                except Exception as e:
                    FaceManager._logger.info(e)

                start = time.time()

            time.sleep(0.1)

