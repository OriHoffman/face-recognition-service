import json
import manager as mng
from flask_restful import reqparse, abort, Api, Resource
from bson import ObjectId
from threading import Lock

post_parser = reqparse.RequestParser()
post_parser.add_argument('vector', type=list, location='json', required=True, help='need face vector')
post_parser.add_argument('name', type=str, location='json', required=True, help="name of vector's person")

get_parser = reqparse.RequestParser()
get_parser.add_argument('vector', type=list, location='json', required=True, help='need face vector')
get_parser.add_argument('amount', type=int, help='number of vectors to return')

lock = Lock()


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class Service(Resource):

    def get(self):
        args = get_parser.parse_args()
        mng.FaceManager.check_vector(args['vector'])
        amount = args.get('amount', 3)
        return {
            'status': 200,
            'result': json.dumps(mng.FaceManager.get_similar_vectors(args['vector'], amount).get_items(), cls=JSONEncoder)
        }

    def post(self):
        args = post_parser.parse_args()
        mng.FaceManager.check_people_count()
        mng.FaceManager.check_vector(args['vector'])
        with lock:
            mng.cache_to_publish.append({'name': args['name'], 'vector': args['vector']})
        return {
            'status': 200,
            'result_obj': {'name': args['name'], 'vector': args['vector']}
        }
