from flask import Flask
from flask_restful import Api
from threading import Thread
from manager import FaceManager
from resources import Service

app = Flask(__name__)
api = Api(app)

api.add_resource(Service, '/service')

Thread(target=FaceManager.dump_to_db, daemon=True).start()  # start posting to db

if __name__ == '__main__':
    app.run(host='0.0.0.0')
